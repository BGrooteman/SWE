'''
Created on 21 mrt. 2016

@author: Gui Generator
'''
import os
import logging
import flask
import functools
import werkzeug
import gevent
import multiprocessing
import queue
import settings
import controller.flaskcontroller as flaskcontroller

app = flask.Flask(__name__)
flask_controller = flaskcontroller.FlaskController(app)
app.config['UPLOAD_FOLDER'] = os.path.join(settings.PATH_TEMP,'upload')
app.config['OUTPUT_FOLDER'] = settings.PATH_OUTPUT

logger = logging.getLogger(__name__)
logger_config_path = os.path.join('config', 'logging.ini')
logger_path = os.path.join('logs')

session = {}

def nocache(f):
    def new_func(*args, **kwargs):
        resp = flask.make_response(f(*args, **kwargs))
        resp.cache_control.no_cache = True
        return resp
    return functools.update_wrapper(new_func, f)

#Starts the server.
#input: debug sets the the debug mode of flask, local changes the behauviour of the interface to react as a local program
#        ,electron determines if the interface is electron, port can specifie a network port. 
def start_local_server_gui(debug=False, port=None):
    #TODO: Add port search and port specified.
    if debug: app.debug = True
    app.run(threaded=True)

    
#Routes to the page for opening bpm files.
@app.route('/')
@app.route('/new', methods=['GET', 'POST'])
@nocache
def new_page():
    if flask.request.method == 'POST':
        output_name = flask.request.form['filename']
        flask_controller._create_folders()
        bpm_file_name = flask_controller._save_bpm(flask.request.files['bpmnfile'])
        output_file_name = output_name + '.html'
        session[output_file_name] = {}
        session[output_file_name]['queue'] = multiprocessing.Queue()
        session[output_file_name]['queue'].put('Start')
        session[output_file_name]['state'] = 'Working'
        input_file_path = os.path.join(app.config['UPLOAD_FOLDER'],bpm_file_name)
        output_file_path = os.path.join(app.config['OUTPUT_FOLDER'],output_file_name)
        p = multiprocessing.Process(target=flaskcontroller._handle_bpm_input, args=(input_file_path,output_file_path,session[output_file_name]['queue'],))
        p.start()
        return flask.render_template('loading.html',outputName=output_file_name)
    else:
        return flask.render_template('new.html')

#Gives a list of outputs or a specific output.
@app.route('/open')
@nocache
def open_page():
    flask_controller._create_folders()
    path = app.config['OUTPUT_FOLDER']
    only_html_files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and f[-5:] == '.html']
    return flask.render_template('open.html',files=only_html_files)

#Returns a window for the output.
@app.route('/open/<path:output_name>')
@nocache
def open_output_page(output_name):
    file_exists = os.path.isfile(os.path.join(app.config['OUTPUT_FOLDER'], output_name))
    output_name_in_session = output_name in session
    if (file_exists and not output_name_in_session) or \
       (output_name_in_session and session[output_name]['state'] == 'Done'):
        return flask.render_template('openOutput.html',outputFileName=output_name)
    elif output_name_in_session:
        return flask.render_template('loading.html',outputName=output_name)
    else:
        return flask.render_template('error.html',errormessage=output_name+".html does not exist.")

#Returns static output pages.
@app.route('/output/<path:filename>', methods=['GET', 'POST'])
@nocache
def output_page(filename):
    path, filename = os.path.split(filename)
    file_folder = os.path.join(app.config['OUTPUT_FOLDER'],path)
    return flask.send_from_directory(file_folder, filename)

#This funtion will stream the state of the program to 
def _loading_event(output_name):
    while True:
        try:
            message = session[output_name]['queue'].get(block=False)
            if message == 'Done': session[output_name]['state'] = 'Done'
        except queue.Empty:
            message = session[output_name]['state']
        yield 'data: ' + message + '\n\n'
        gevent.sleep(0.1)
    
@app.route('/loading/', methods=['GET', 'POST'])
@nocache
def stream():
    output_name = flask.request.args['output']
    return flask.Response(_loading_event(output_name), mimetype="text/event-stream")

#Gives a export function
@app.route('/export')
@nocache
def export_page():
    return flask.render_template('error.html',errormessage="Export function not yet supported.")
