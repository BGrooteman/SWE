var branchCount = 0;
var finishedBranches = 0;

// adds a listener for the iframes on current page
function addIFrameListener() {
	if ('addEventListener' in window){
		window.addEventListener('message',iFrameListener, true);
	} else if ('attachEvent' in window){//IE
		window.attachEvent('on'+'message',iFrameListener);
	}
}

// appends a resume button with the given action as redirect target
function addResumeButton(action) {
    // create button and containers
    var formGroup = document.createElement('div');
    var buttonContainer = document.createElement('div');
    var button = document.createElement('button');
    var row = document.createElement('div');

    // set classes for styling
    row.setAttribute('class', 'row');

    formGroup.setAttribute('class', 'form-group');

    buttonContainer.setAttribute('class', 'col-sm-offset-2 col-sm-10');
    buttonContainer.setAttribute('align', 'left');

    // set button styling and redirect location
    button.setAttribute('type', 'button');
    button.setAttribute('class', 'btn btn-primary');
    button.innerHTML = 'Continue';
    button.setAttribute('onclick', "window.location='"+action+".html'");

    // nest the button in the containers and append to form on page
    buttonContainer.appendChild(button);
    formGroup.appendChild(buttonContainer);
    row.appendChild(formGroup);

    document.getElementById('form').appendChild(row);
}

// process message received from iframe
function iFrameListener(event) {
    console.log(event);
    var message = event.data.split(" ");
    console.log(message);
	if (message[0] == "end") {
		finishedBranches = finishedBranches + 1;
		console.log(finishedBranches);
		if(finishedBranches == branchCount){
		    addResumeButton(message[1]);
		}
	}
}

// reload all iframes on page. This is required because of a
// persistent firefox bug where iframes do not return to their
// source on page reload
function reloadiFrames(){
    iFrames = document.getElementsByTagName('iframe');
    for(i=0;i<iFrames.length;i++){
        iFrames[i].src = iFrames[i].src;
    }
}

