'''
Created on 17 Mar 2016

@author: guigenerator team
'''
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import settings
import view.htmlrenderer.htmlrenderer as renderer
import view.htmlrenderer.htmlrendables as rendables

if __name__ == '__main__':
    render_list = []
    render_list.append(rendables.ActivityRendable('01','Start','02'))
    render_list.append(rendables.SubProcessRendable('02','SubProcess','05','03'))
    render_list.append(rendables.ActivityRendable('03','StartSub','04'))
    render_list.append(rendables.EndRendable('04','EndSub'))
    render_list.append(rendables.EndRendable('05','End'))
    html = renderer.HTMLRenderer(render_list,os.path.join(settings.PATH_OUTPUT_TESTS,'subprocesstest.html'))
    html.render_to_html()