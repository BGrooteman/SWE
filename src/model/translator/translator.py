"""
Created on 18 March. 2016

@author: Team GuiGenerator

This script will translate the whole folder into HtmlRendable objects,
by calling the diagramtranslator for each diagram.

"""
import model.translator.diagramtranslator as DT
import view.htmlrenderer.htmlrenderer as Renderer
import logging

class Translator: 

    def __init__(self, parser, output_file_path):
        self.logger = logging.getLogger(__name__)
        rendables_list = []
        start_list = []

        for index in range(parser.number_of_diagrams):
            diag_translator = DT.DiagramTranslator(parser.get_diagram_info(index))
            new_rendables, start_id, start_description = diag_translator.generate_forms()
            rendables_list.extend(new_rendables)
            start_list.append((start_id, start_description))
        
        renderer = Renderer.HTMLRenderer(rendables_list, output_file_path, starts=start_list)
        renderer.render_to_html()
