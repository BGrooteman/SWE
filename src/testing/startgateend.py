'''
Created on 10 Mar 2016

@author: Nico
'''
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import settings
import view.htmlrenderer.htmlrendables as htmlrendables
import view.htmlrenderer.htmlrenderer as htmlrenderer

if __name__ == '__main__':
    start = htmlrendables.ActivityRendable('01','This is the start of the test form.','02')
    gate = htmlrendables.GateRendable('02','Answer yes or no.',['Yes','No'],['03','04'])
    end1 = htmlrendables.EndRendable('03','You have chosen yes.')
    end2 = htmlrendables.EndRendable('04','You have chosen no.')
    html = htmlrenderer.HTMLRenderer([start,gate,end1,end2],os.path.join(settings.PATH_OUTPUT_TESTS,'gatetest.html'))
    html.render_to_html()