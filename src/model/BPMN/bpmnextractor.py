"""
Created on 4 March 2016

@author: Team GuiGenerator

These functions will the .bpm file and extract the .diag files in a temp folder.
After the parsing is done the temp folder can be deleted
"""
import logging
import zipfile as ZF
import os
import shutil
import glob
import settings


class BpmnExtractor:

    def __init__(self,filename):
        self.logger = logging.getLogger(__name__)
        filename_no_extension = os.path.splitext(filename)[0]
        self.path = os.path.join(settings.PATH_TEMP,filename_no_extension)
        self.number_of_subfiles = 0
        self.opened = False
        self.filename = filename
        self.open()

    # Input: filepath of the .bpm file
    # Output: - (Files will be extracted in temp-folder)
    def open(self):
        self.logger.debug("Extracting {} in temp".format(self.filename))
        with ZF.ZipFile(self.filename, 'r') as diagramFile:
            diagramFile.extractall(path=self.path)                    # Extract the .diagram in the temp folder

        diag_files = glob.glob(os.path.join(self.path,'*.diag'))
        count_of_files = 0
        for subfilename in diag_files:
            new_name = os.path.join(self.path,'subfolder{}'.format(count_of_files))
            self.logger.info("Now unpacking into {}".format(new_name))

            with ZF.ZipFile(subfilename, 'r') as diagramSubFile:
                diagramSubFile.extractall(path=new_name)
            count_of_files += 1
        self.number_of_subfiles = count_of_files                    # Since it is 0-index, the nr of files is +1
        self.opened = True

    # Input: -
    # Output: the number of .diag files in the .bpm file.
    def get_nr_subfiles(self):
        if not self.opened:
            # TODO User tries to get path when file is not yet extracted
            # Maybe call a self-defined exception here (Cant give xml_path if file is not yet extracted)
            raise NotImplementedError("No nice exception yet, but can not give number of diagrams")
        return self.number_of_subfiles

    # This function returns the paths to de diagram.xml files.
    # Input: -
    # Output: the paths to the Diagram.xml files.
    def get_paths(self):
        if not self.opened:
            # TODO User tries to get path when file is not yet extracted
            # Maybe call a self-defined exception here (Cant give xml_path if file is not yet extracted)
            raise NotImplementedError("No nice exception yet, but can not give path")
        all_paths = []
        for index in range(self.number_of_subfiles):
            all_paths.append(os.path.join(self.path,'subfolder{}'.format(index),'Diagram.xml'))
        return all_paths

    def close(self):
        shutil.rmtree(path=self.path)  # Remove temp folder when done
        self.number_of_subfiles = 0
        self.opened = False
        




