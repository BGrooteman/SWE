GUIGENERATOR
==========
This is the README file for the project we have to do for the course Software Engineering.
The project is a GUI Generator. The program takes .bpm files and translates them into HTML pages with forms on it.


# Dependencies
The project makes use of a couple of libraries that need to be installed before the code can be ran: 
* [lxml](http://lxml.de/)
* [Jinja2](http://jinja.pocoo.org/docs/dev/)
* [Flask] (http://flask.pocoo.org/)
* [gevent] (http://www.gevent.org/)
* [Electron](http://electron.atom.io/)
Complete instructions are in the installation manual.

# Features: 
- [x] Activities
- [x] Choice Gates
- [x] Subprocesses
- [x] Parallel Gates
- [x] Messages
- [ ] Enrichment file