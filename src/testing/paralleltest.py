'''
Created on 17 Mar 2016

@author: guigenerator team
'''
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import settings
import view.htmlrenderer.htmlrenderer as renderer
import view.htmlrenderer.htmlrendables as rendables

if __name__ == '__main__':
    render_list = []
    render_list.append(rendables.ActivityRendable('01','Start','02'))
    render_list.append(rendables.StartParallelGateRendable('02',['03','05','06']))
    render_list.append(rendables.ActivityRendable('03','Parallel 1-1','04'))
    render_list.append(rendables.ActivityRendable('04','Parallel 1-2','07'))
    render_list.append(rendables.ActivityRendable('05','Parallel 2','07'))
    render_list.append(rendables.ActivityRendable('06','Parallel 3','07'))            
    render_list.append(rendables.EndParallelGateRendable('07','08'))          
    render_list.append(rendables.EndRendable('08','End'))
    html = renderer.HTMLRenderer(render_list,os.path.join(settings.PATH_OUTPUT_TESTS,'paralleltest.html'))
    html.render_to_html()