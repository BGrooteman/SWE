"""
Created on 27 feb. 2016

@author: Team guigenerator

This class extracts, parses and translates the input. It first extracts the .BPM
file using the Extractor, then it uses these extracted files to parse the input
, this parsed input is then send to the Translator which transforms tha graph,
into a flow of forms.
"""
import logging
import model.BPMN.bpmnextractor as BE
import model.BPMN.bpmnparser as BP
import model.translator.translator as TR 

# Input: Filepath of the .bpm file
# Output: Nothing, it sends the parsed file to the HTLMRender
def handle_input(file_path, output_file_path):
    logger = logging.getLogger(__name__)
    logger.debug("Reading at {}". format(file_path))
    extractor = BE.BpmnExtractor(file_path)
    parser = BP.BpmnParser(extractor)
    TR.Translator(parser, output_file_path)
    extractor.close()
        