function setResumeAction(action, remainingPoints) {
    document.getElementById('form').action = action + ".html?sub=" + remainingPoints;
}

// look for the element named "subProcess" and if it exists, return its
// id as a sub resume point
function getSubContinuePoint() {
    elements = document.getElementsByName('subProcess');
	return elements[0].getAttribute('id');
}

// append a new subprocess resume point to the end of the currently known
// resume points
function addSubResumePoint(resumePoint) {
    var resumePoints = sessionStorage.resumePoints;
	if(resumePoints != '') {
		resumePoints = resumePoints + '|' + resumePoint;
	}else{
		resumePoints = resumePoint;
	}
    sessionStorage.resumePoints = resumePoints;
}

// remove the last (most recently added) subprocess resume point
function getLastPoint(){
    var resumePoints = sessionStorage.resumePoints;
    var pointsArray = resumePoints.split('|');
	var remainingPoints = '';
    for(i=0;i<pointsArray.length-1;i++){
        remainingPoints = remainingPoints + pointsArray[i];
    }
    sessionStorage.resumePoints = remainingPoints;
    return pointsArray[pointsArray.length-1];
}

// appends a resume button with the given action as redirect target
function addResumeButton() {
    var formGroup = document.createElement('div');
    var buttonContainer = document.createElement('div');
    var button = document.createElement('button');
    var row = document.createElement('div');
    row.setAttribute('class', 'row');

    formGroup.setAttribute('class', 'form-group');

    buttonContainer.setAttribute('class', 'col-sm-offset-2 col-sm-10');
    buttonContainer.setAttribute('align', 'left');

    button.setAttribute('type', 'submit');
    button.setAttribute('class', 'btn btn-primary');
    button.innerHTML = 'Continue outside subprocess';

    buttonContainer.appendChild(button);
    formGroup.appendChild(buttonContainer);
    row.appendChild(formGroup);

    document.getElementById('form').appendChild(row);
}

// called from every "End" node, if there are resumepoints left, we must
// be at the end of a subprocess, so we add a button to resume the parent process
// if we are not within a subprocess, then nothing happens and the process terminates
function reachedEnd() {
    var resumePoints = sessionStorage.resumePoints;
    if(resumePoints != '') {
        action = getLastPoint(resumePoints);
        setResumeAction(action);
        addResumeButton();
    }
}

// called from a subprocess start node. We append the resume point of this subprocess to
// the end of the list of resume points
function handleSubProcess() {
	var point = getSubContinuePoint();
	addSubResumePoint(point);
}
