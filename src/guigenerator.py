'''
Created on 24 feb. 2016

@author: Team GuiGenerator

This is the main file called by the user. It has no other function than to
delegate the systemarguments and initialize the logger
'''
import os
import sys
import multiprocessing
import logging.config
import settings
import controller.arguments as arguments

logger_config_path = os.path.join(settings.PATH_CONFIG, 'logging.ini')
logger_path = os.path.join(settings.PATH_LOG, 'guigen.log')

def _logger_init():
    os.makedirs(os.path.dirname(logger_path), exist_ok=True)
    logging.config.fileConfig(logger_config_path, disable_existing_loggers=False, defaults={'logfilename': logger_path})

if __name__ == '__main__':
    # On Windows calling this function is necessary for the install script.
    multiprocessing.freeze_support()
    
    # Initialize logger
    _logger_init()
    
    # Call the command line arguments controller.
    arguments.parse_commands(sys.argv)
