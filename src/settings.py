import os
PROJECT_ROOT = os.path.dirname(__file__)
PATH_TEMP = os.path.join(PROJECT_ROOT, 'temp')
PATH_OUTPUT = os.path.join(PROJECT_ROOT, 'output')
PATH_OUTPUT_TESTS = os.path.join(PROJECT_ROOT, 'testoutput')
PATH_LOG = os.path.join(PROJECT_ROOT, 'logs')
PATH_CONFIG = os.path.join(PROJECT_ROOT, 'config')