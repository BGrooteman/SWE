'''
Created on 24 feb. 2016

@author: Team GuiGenerator

This script will render our model to html.
Input:
    List of HTMLRendables and starting points.
Output:
    A folder filled with html files.
'''
import os
import logging
import shutil
from jinja2 import Environment, FileSystemLoader
from operator import itemgetter
import settings

class HTMLRenderer():
    
    #Input: A list of htmlRendables, a filepath and the optional argument to starts which is a two dimensional array in
    #         which the first column are the start id and the second column the the description for the links.
    def __init__(self, html_rendables, file_path, starts = None):
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Function arguments are: htmlRendables = {}, filePath = {}'.format(html_rendables, file_path))
        self.html_rendables = html_rendables
        self.file_path = file_path
        self.starts = starts
        if self.starts != None:
            self.starts.sort(key=itemgetter(1))
    
    #Copies a fold to file
    def _copy_folder(self, path, file_name, folder):
        directory = os.path.dirname(__file__)
        folderin = os.path.join(directory, 'templates', folder)
        folderout = os.path.join(path, file_name, folder)
        self.logger.info('Copying {} folder.'.format(folder))
        shutil.copytree(folderin, folderout)

    #Writes the start page to file.
    def _write_start_page_to_file(self, file_name):
        with open(self.file_path, "w") as f:
            self.logger.info('Writing {} to file.'.format(self.file_path))
            template_path = os.path.join(settings.PROJECT_ROOT,'view', 'htmlrenderer', 'templates')
            env = Environment(loader=FileSystemLoader(template_path))
            if self.starts == None:
                self.logger.debug('Writing a single redirect.')
                template = env.get_template('RedirectForm.html')
                f.write(template.render(title=file_name, filename=file_name, start_form_name=self.html_rendables[0].name))
            elif len(self.starts) == 1:
                self.logger.debug('Writing a single redirect.')
                template = env.get_template('RedirectForm.html')
                f.write(template.render(title=file_name, filename=file_name, start_form_name=self.starts[0][0]))
            else:
                self.logger.debug('Writing a list of start buttons.')
                template = env.get_template('ViewForm.html')
                f.write(template.render(title=file_name, filename=file_name, starts=self.starts))
                
    #Loops through the rendables list and writes them to file.
    def _write_rendables_to_file(self, path, file_name):
        for rendable in self.html_rendables:                                                # Loops through all HtmlRendables.
            rendable_file_path = os.path.join(path, file_name, rendable.name + ".html")  	# Creates the filepath for the HtmlRendable.
            os.makedirs(os.path.dirname(rendable_file_path), exist_ok=True)
            with open(rendable_file_path, "w") as f:
                self.logger.info('Writing {} to file.'.format(rendable_file_path))
                f.write(rendable.render())

    #Create the folders and empties the filename folder for the htmlfiles.
    def _create_folders(self,path,file_name):
        if path != '': os.makedirs(path, exist_ok=True)             # Creates the directories if they don't exist.
        file_path = os.path.join(path,file_name)
        if os.path.exists(file_path):
            shutil.rmtree(file_path)

    #The function will render the HtmlRendables to files.
    def render_to_html(self):
        path, file_name = os.path.split(self.file_path)             # Splits the directories from the filename.
        file_name = file_name.split(".")
        file_name = ''.join(file_name[:-1])                         #Removes the file-type from the filename
        self._create_folders(path,file_name)
        self._copy_folder(path, file_name, 'static')
        self._write_start_page_to_file(file_name)
        self._write_rendables_to_file(path, file_name)
