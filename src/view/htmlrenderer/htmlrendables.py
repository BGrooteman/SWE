'''
Created on Feb 28, 2016

@author: Team guigenerator

This script is a collection of HtmlRendable classes. All of the classes inherit from the HtmlRendable class.
These classes are a wrapper for the jinja2 templates that generate the form pages.
Input:
    The Data within the bpmn model primitives.
Output:
    Html pages which represent those primitives.
'''
import os
import logging
from jinja2 import Environment, FileSystemLoader
import settings


class HtmlRendable:
    # Don't fill in the methods this is an abstract class.
    
    def __init__(self, name):
        self.logger = logging.getLogger(__name__)
        self.name = name
        template_path = os.path.join(settings.PROJECT_ROOT,'view', 'htmlrenderer', 'templates')
        self.env = Environment(loader=FileSystemLoader(template_path))
       
    # This abstract method should return a complete html string.
    # Output: Returns a string with html code.
    def render(self):
        self.logger.warning("This is an abstract function.")
        raise NotImplementedError("This is an abstract function.")
        return ""

#This class renders a text and radio buttons.
class GateRendable(HtmlRendable):
    
    # Renders a gate into html.
    # Input: primitive_id is the id of the gate, question is the question asked at the gate
    #        , answers are the names of the outgoing connections, next_primitives are the id connected to the answers.
    def __init__(self,primitive_id, question, answers, next_primitives):
        HtmlRendable.__init__(self,primitive_id)
        self.question = question
        self.answers = answers
        self.redirects = next_primitives
    
    # Output: Returns the html representation of a gate.
    def render(self):
        self.logger.debug('Function called.')
        template = self.env.get_template('Gate.html')
        return template.render(title = self.name, header = self.question, answers = zip(self.answers,self.redirects))
    
#This class renders the start of a parallel gate.
class StartParallelGateRendable(HtmlRendable):
    
    # Renders a parallel gate into html.
    # Input: primitive_id is the id of the parallel gate, next_primitives is a list of successor id
    #        ,text is an optional text enrichment.
    def __init__(self,primitive_id, next_primitives, text=''):
        HtmlRendable.__init__(self,primitive_id)
        self.text = text
        self.redirects = next_primitives
        
    # Output: Returns the html representation of a parallel gate.
    def render(self):
        self.logger.debug('Function called.')
        template = self.env.get_template('ParalelProcess.html')
        return template.render(title = self.name, text = self.text, paralelBranches=self.redirects)
    
#This class renders the end of a parallel gate.
class EndParallelGateRendable(HtmlRendable):
    
    # Renders a parallel gate into html.
    # Input: primitive_id is the id of the parallel gate, next_primitive is the id of the successor
    #        ,text is an optional text enrichment.
    def __init__(self,primitive_id, next_primitive, text=''):
        HtmlRendable.__init__(self,primitive_id)
        self.text = text
        self.redirect = next_primitive
        
    # Output: Returns the html representation of a parallel gate.
    def render(self):
        self.logger.debug('Function called.')
        template = self.env.get_template('ParalelProcessEnd.html')
        return template.render(title = self.name, header = self.text, redirect = self.redirect)

# This class renders start, incoming message and activity primitives to html.
class ActivityRendable(HtmlRendable):
        
    # Renders a activity event into html.
    # Input: primitive_id is the id of the event, text is the text that has to be displayed
    #        , next is the successor, message is the name of the primitive that sends a message
    #        , message_id is the id of the primitive that has send a message.
    def __init__(self,primitive_id,text,next_primitive,message=None,message_id = None):
        HtmlRendable.__init__(self, primitive_id)
        self.text = text
        self.next_page = next_primitive
        self.message = message
        self.message_id = message_id
            
    # Output: Returns the html representation of a Activity event.
    def render(self):
        self.logger.debug('Function called.')
        template = self.env.get_template('Activity.html')
        ren = template.render(title = self.name, text = self.text, redirect = self.next_page, message=self.message, messagelink=self.message_id)
        return ren

#The class renders an outgoing message to html.
class MessageRendable(ActivityRendable):
    
    # Input: primitive_id is the id of the event, text is the text that has to be displayed
    #        , next_primitive is the successor, message is the name of the primitive that it sends a message to
    #        , message_id is the id of the primitive that it sends a message to.
    def __init__(self,primitive_id,text,next_primitive,message,message_id = None):
        ActivityRendable.__init__(self, primitive_id, text, next_primitive,message,message_id)
        
    # Output: Returns the html representation of a Message event.
    def render(self):
        self.logger.debug('Function called.')
        template = self.env.get_template('Message.html')
        ren = template.render(title = self.name, text = self.text, redirect = self.next_page, message=self.message, messagelink=self.message_id)
        return ren

#The class renders an sub process activity to html.
class SubProcessRendable(ActivityRendable):
    
    # Input: id is the id of the event, text is the text that has to be displayed
    #        , next_primitive is the successor, subprocess_id is the id of the start node of the subprocess.
    def __init__(self,primitive_id,text,next_primitive,subprocess_id):
        ActivityRendable.__init__(self, primitive_id, text, next_primitive)
        self.subprocess = subprocess_id
        
    # Output: Returns the html representation of a SubProcess Activity event.
    def render(self):
        self.logger.debug('Function called.')
        template = self.env.get_template('SubProcess.html')
        ren = template.render(title = self.name, text = self.text, redirect = self.next_page, subredirect=self.subprocess)
        return ren

# This class renders an end point of the form flow.
class EndRendable(HtmlRendable):
        
    # Renders a end event into html.
    # Input: primitive_id is the id of the event, text is the text that has to be displayed.
    #        , message is the name of the primitive that sends a message
    #        , message_id is the id of the primitive that has send a message.
    def __init__(self,primitive_id,text,message=None,message_id=None):
            HtmlRendable.__init__(self,primitive_id)
            self.text = text
            self.message = message
            self.message_id = message_id
            
    # Output: Returns the html representation of a start event.
    def render(self):
            self.logger.debug('Function called.')
            template = self.env.get_template('End.html')
            return template.render(title = self.name, text = self.text, message=self.message, messagelink=self.message_id)