'''
Created on 18 mrt. 2016

@author: guigenerator team
'''
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import settings
import view.htmlrenderer.htmlrenderer as renderer
import view.htmlrenderer.htmlrendables as rendables

if __name__ == '__main__':
    render_list = []
    render_list.append(rendables.ActivityRendable('01','Start','02'))
    render_list.append(rendables.MessageRendable('02','Message test','05',message='Start other',message_id='03'))
    render_list.append(rendables.ActivityRendable('03','Start other','04',message='Message test', message_id='02'))
    render_list.append(rendables.EndRendable('04','End other'))
    render_list.append(rendables.EndRendable('05','End'))
    html = renderer.HTMLRenderer(render_list,os.path.join(settings.PATH_OUTPUT_TESTS,'messagetest.html'))
    html.render_to_html()