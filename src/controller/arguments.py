'''
Created on 24 feb. 2016

@author: Team guigenerator

This script parses command line arguments and calls the approriate functions 
to start the main functionality of the program.
'''

import logging
import controller.inputHandler as inputHandler
from view.htmlgui import start_local_server_gui

# Input: A list of strings that contain the cmdline arguments
# When arguments are given the commandline, the program goes into the parsing
# directly. In the case of no arguments at all, the program will start the Flask
# server and Electron* and give the user a GUI to input the files.
def parse_commands(arguments):
    logger = logging.getLogger(__name__)
    logger.debug('Function argument is: arguments = {}'.format(arguments))
    input_path = None
    output_path = None
    flask_debug = False
    flask_port = None
    for argument in arguments[1:]:                              # First argument is the function call, skip that)
        if argument[0:6] == "input:":
            input_path = argument[6:]
        elif argument[0:7] == "output:":
            output_path = argument[7:]
        elif argument[:6] == 'flask:' and argument[6:] == 'debug':
            flask_debug = True
        elif argument[:10] == 'flaskport:':
            flask_port = int(argument[10:])
    if input_path is not None and output_path is not None:
        inputHandler.handle_input(input_path, output_path)
    else:
        start_local_server_gui(flask_debug,flask_port)
