'''
Created on 24 mrt. 2016

@author: GUI team
'''
import unittest
from collections import defaultdict
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import settings
import model.BPMN.bpmnextractor as BE
import model.BPMN.bpmnparser as BP

class ParserTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.maxDiff = None
        self.extractor = BE.BpmnExtractor(os.path.join(settings.PROJECT_ROOT,'testbpm','PartyProcess.bpm'))
        self.parser = BP.BpmnParser(self.extractor)
    
    @classmethod
    def tearDownClass(self):
        self.extractor.close()

    def test_parser_graph_1(self):
        graph = self.parser.get_diagram_info(0)["Graph"]
        answer = defaultdict(list)
        answer['bdc2b5ec-5b19-4074-a24f-10705ccda40c'] = [('8c42b86f-75ee-444b-b973-04a6a33d7b27', '')]
        answer['946b11cb-d980-4046-9d61-051dfabebab4'] = [('bdc2b5ec-5b19-4074-a24f-10705ccda40c', '')]
        self.assertEqual(graph, answer)
        
    def test_parser_graph_2(self):
        graph = self.parser.get_diagram_info(1)["Graph"]
        answer2 = defaultdict(list)
        answer2['a5fffc42-c9fa-480e-84d7-0b6b8f6bb070'] = [('644a6a7e-a006-44ed-a8d7-8b7d8331ed0c', '')]
        answer2['5017682f-7f04-423a-a536-9fcd5a957611'] = [('d375974f-42fc-4425-b2a5-ff2ceef8260f', ''), ('daee3f80-ce55-4735-a89a-0de87be7e02c', '')]
        answer2['833e468c-61fe-4a68-bdea-d549e3edab67'] = [('a5fffc42-c9fa-480e-84d7-0b6b8f6bb070', '')]
        answer2['031bfa67-ca24-4e68-ba7f-05f48605dfd9'] = [('2ed744d0-7944-4cdf-ae84-98a75d007632', '')]
        answer2['e44a3147-2170-4e34-a232-0328d7425b3c'] = [('a5fffc42-c9fa-480e-84d7-0b6b8f6bb070', '')]
        answer2['ac2ed070-7e8f-407e-a6b6-141647197ae7'] = [('0d157781-a94a-4758-89c7-7bc9159560ea', '')]
        answer2['d375974f-42fc-4425-b2a5-ff2ceef8260f'] = [('ba038705-0fe5-4ed4-be41-81de9770d875', '')]
        answer2['ba038705-0fe5-4ed4-be41-81de9770d875'] = [('496c3357-f65d-482c-93e7-6f5bc56bbd2c', '')]
        answer2['daee3f80-ce55-4735-a89a-0de87be7e02c'] = [('ba038705-0fe5-4ed4-be41-81de9770d875', '')]
        answer2['2ed744d0-7944-4cdf-ae84-98a75d007632'] = [('ac2ed070-7e8f-407e-a6b6-141647197ae7', '')]
        answer2['e9bb3f12-26b3-451e-b888-0b52e3f30556'] = [('b2301923-8a45-438d-8db2-e4e8f5b66ee7', '')]
        answer2['58975c46-b6af-4b58-95f6-0b980cfb5c8b'] = [('5017682f-7f04-423a-a536-9fcd5a957611', '')]
        answer2['b2301923-8a45-438d-8db2-e4e8f5b66ee7'] = [('a35b645e-83a2-4269-a45f-25fc00040aaf', '')]
        answer2['f7d244de-35e2-4c2c-870c-6456900e1ea2'] = [('9d126288-422a-41e3-aeb4-ba13d4234218', '')]
        answer2['0d157781-a94a-4758-89c7-7bc9159560ea'] = [('833e468c-61fe-4a68-bdea-d549e3edab67', ''), ('e44a3147-2170-4e34-a232-0328d7425b3c', '')]
        answer2['1abe4d30-10e7-4abc-baea-018e690841e6'] = [('05f11820-3625-4b2e-a4de-47bbd18b353e', '')]
        answer2['644a6a7e-a006-44ed-a8d7-8b7d8331ed0c'] = [('e9bb3f12-26b3-451e-b888-0b52e3f30556', 'Yes'), ('f7d244de-35e2-4c2c-870c-6456900e1ea2', 'No')]
        answer2['05f11820-3625-4b2e-a4de-47bbd18b353e'] = [('58975c46-b6af-4b58-95f6-0b980cfb5c8b', '')]
        self.assertEqual(graph, answer2)        
        
    def test_parser_dict_1(self):
        dict = self.parser.get_diagram_info(0)["Dict"]
        answer = {}
        answer['946b11cb-d980-4046-9d61-051dfabebab4'] = {'Type': 'Activity', 'Name': 'Main Start',
                                                          'message_received_name': None, 'message_received_id': None}
        answer['8c42b86f-75ee-444b-b973-04a6a33d7b27'] = {'Type': 'Activity', 'Name': 'Main End',
                                                          'message_received_name': None, 'message_received_id': None}
        answer['bdc2b5ec-5b19-4074-a24f-10705ccda40c'] = {'Type': 'SubProcess', 'Name': 'Subprocess in main',
                                                          'message_received_name': None, 'message_received_id': None}

        self.assertEqual(dict, answer)
    
    def test_parser_start_id(self):
        start_id1 = self.parser.get_diagram_info(0)["Start_id"]
        start_id2 = self.parser.get_diagram_info(1)["Start_id"]
        self.assertEqual(start_id1, "946b11cb-d980-4046-9d61-051dfabebab4")
        self.assertEqual(start_id2, "031bfa67-ca24-4e68-ba7f-05f48605dfd9")
    
    def test_parser_start_nodes(self):
        starts1 = self.parser.get_diagram_info(0)["Start_nodes"]
        starts2 = self.parser.get_diagram_info(1)["Start_nodes"]
        self.assertEqual(starts1, [('946b11cb-d980-4046-9d61-051dfabebab4', 'None')])
        self.assertEqual(starts2, [('031bfa67-ca24-4e68-ba7f-05f48605dfd9', 'None'), ('1abe4d30-10e7-4abc-baea-018e690841e6', 'Message')])
    
    def test_parser_name(self):
        name1 = self.parser.get_diagram_info(0)["Name"]
        name2 = self.parser.get_diagram_info(1)["Name"]
        self.assertEqual(name1, "main")
        self.assertEqual(name2, "Sub1")
    
    def test_parser_sub_links(self):
        sub_links = self.parser.get_diagram_info(1)["Sub_links"]
        self.assertEqual(sub_links, {'bdc2b5ec-5b19-4074-a24f-10705ccda40c': '031bfa67-ca24-4e68-ba7f-05f48605dfd9'})
        
    def test_parser_messages(self):
        message = self.parser.get_diagram_info(1)["Messages"]
        answer = {'2ed744d0-7944-4cdf-ae84-98a75d007632': {'Name': 'Message', 'Id': 'c72a5469-5fce-421c-9677-c17bb4e4758f', 'Source': '2ed744d0-7944-4cdf-ae84-98a75d007632', 'Target': '1abe4d30-10e7-4abc-baea-018e690841e6'}, '58975c46-b6af-4b58-95f6-0b980cfb5c8b': {'Name': 'Party Invitation', 'Id': 'f4cb7621-822a-4769-892e-8e1122ebe91a', 'Source': '58975c46-b6af-4b58-95f6-0b980cfb5c8b', 'Target': 'b2301923-8a45-438d-8db2-e4e8f5b66ee7'}}
        self.assertEqual(message, answer)
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()