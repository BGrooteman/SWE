"""
Created on 24 feb. 2016

@author: Team GuiGenerator

This script will parse .xml into a dictionary, a graph and a list of messages.
lxml is used instead of elementtree because of the namespaces and the xpath support.
The xml file is build up in peculiar way, therefore there is no good way to make the code more
abstract. (It is NOT the case that every activity has an "implementation" child which tells us what
its type is)

Input:
    file_path: The path of the temp-folder in which the temporary files are extracted
Ouput:
    All the public functions which return the extracted information from the file

"""
from lxml import etree, objectify  # use lxml over ElementTree because of namespaces
import logging
from collections import defaultdict


class DiagramParser:
    def __init__(self, file_path):
        self.logger = logging.getLogger(__name__)
        parser = etree.XMLParser(remove_blank_text=True)
        self.tree = etree.parse(file_path, parser)
        self.root = self.tree.getroot()
        self.__remove_namespaces()

        self.name = self.root.attrib["Name"]
        self.activities = []            # Will be a list of dicts with keys: {'id', 'name'}
        self.sub_processes = []         # List of the subprocesses

        self.connections = []           # List of dicts with keys: {'name', 'id', 'from', 'to'} (name is optional)
        self.leftover_connections = []  # All the connections that miss a key.

        self.messages = {}              # Hash with key: sourceId and value: the message object
        self.leftover_messages = []     # All the messages that miss a key

        self.start = []                 # List of start-nodes (Only one in most cases)
        self.end = []                   # List of end-nodes

        self.parallel_gateways = []     # List of parallel_gateways with extra Attrib "Opens"
        self.choice_gateways = []       # List of choice_gateways

        self.connection_graph = defaultdict(list)      # Graph denotes connections between Id's
        self.graph_made = False

        self.activity_dict = {}         # Dict gives the attributes as value and has Id as a key
        self.dict_made = False

    # Removes the namespaces from the xml by finding end of namespace tag and taking the text behind it
    def __remove_namespaces(self):
        for elem in self.root.getiterator():
            i = elem.tag.find('}')
            if i >= 0:
                elem.tag = elem.tag[i+1:]
        objectify.deannotate(self.root, cleanup_namespaces=True)

    # Returns the (startNode, idOfPool) found in the xml tree. True start is the StartEvent that None as Trigger
    def __find_start_tuple(self):
        true_start = self.root.xpath(".//Activity[Event/StartEvent/@Trigger = 'None']")

        pool_id = self.root.xpath(".//Pool[@BoundaryVisible='true']")
        return true_start[0].attrib["Id"], pool_id[0].attrib['Process']

    # Gets starts nodes with their triggers (Trigger "None" is the true start)
    def __find_all_start(self):
        start = self.root.xpath(".//Activity[Event/StartEvent]")
        triggers = self.root.xpath(".//Activity/Event/StartEvent/@Trigger")
        for node, trigger in zip(start, triggers):
            self.start.append((node.attrib['Id'], trigger))

    # Finds the Transition elements, that connect Activities. Transitions are the edges in the graph
    def __find_connections(self):
        all_connections = self.tree.findall(".//Transition")
        for connection in all_connections:
            if "From" in connection.attrib and "To" in connection.attrib:
                """
                if "Name" not in connection.attrib and self.dict_made:
                    self.logger.error("The connection between {} and {} has no name".format(self.activity_dict[
                                        connection.attrib['From']], self.activity_dict[connection.attrib["To"]]))
                elif "Name" not in connection.attrib:
                    self.__make_dict()
                    self.logger.error("The connection between {} and {} has no name".format(self.activity_dict[
                                        connection.attrib['From']], self.activity_dict[connection.attrib["To"]]))
                """
                self.connections.append(connection)
            else:
                self.leftover_connections.append(connection)

    # Finds the Transition elements, that connect Activities. Transitions are the edges in the graph
    def __find_messages(self):
        all_messages = self.tree.findall(".//MessageFlow")

        for message in all_messages:
            if "Target" in message.attrib and "Source" in message.attrib:
                src = message.attrib["Source"]
                target = message.attrib["Target"]
                self.messages[src] = message.attrib

                self.activity_dict[target]["message_received_id"] = src
                self.activity_dict[target]["message_received_name"] = self.activity_dict[src]["Name"]

            else:
                self.leftover_messages.append(message)

    # Finds the elements of self.activities that are also in selector_list and overwrites the "Type" attribute with
    # type_name
    def __add_attrib(self, selector_list, key_name, type_name):
        for activity in self.activities:
            for item in selector_list:
                if activity == item:
                    activity.attrib[key_name] = type_name

    # Finds ALL the Activities in the XML, these are the vertices in the graph
    def __find_activities(self):
        self.activities = self.root.xpath('.//Activity')
        for a in self.activities:
            a.attrib["Type"] = "Activity"

    # Finds the activities that have a THROW event (Throw a message)
    def __find_message_activities(self):
        message_throw_activities = self.root.xpath('.//Activity[Event/IntermediateEvent/TriggerResultMessage/'
                                                   '@CatchThrow="THROW"]')
        self.__add_attrib(message_throw_activities, "Type", "Message")

    # Finds all the activities that point to sub-processes
    def __find_sub_processes(self):
        sub_processes = self.root.xpath('.//Activity[Implementation/SubFlow]')
        sub_processes_pointers = self.root.xpath('//Activity/Implementation/SubFlow')

        for activity in self.activities:
            for idx in range(len(sub_processes)):
                if activity == sub_processes[idx]:
                    activity.attrib["Type"] = "SubProcess"
                    if 'Id' not in sub_processes_pointers[idx].attrib:
                        self.logger.error("No subprocess specified in {} activity {}".format(self.name,
                                                                                             activity.attrib['Name']))
                    else:
                        activity.attrib["PointId"] = sub_processes_pointers[idx].attrib['Id']
                        # Add the pair Id and Subprocess
                        self.sub_processes.append((activity.attrib['Id'], sub_processes_pointers[idx].attrib['Id']))

    # Finds all the gateways that have a route child. This also includes the parallel_gateways
    def __find_choice_gateways(self):
        choice_gateways = self.root.xpath(".//Activity[Route]")
        self.__add_attrib(choice_gateways, "Type", "ChoiceGateway")

    # Finds all the gateways where the type is parallel, overwrites the attributes from the choice gateway
    def __find_parallel_gateways(self):
        parallel_gateways = self.root.xpath(".//Activity[Route[@GatewayType='Parallel']]")
        self.__add_attrib(parallel_gateways, "Type", "ParallelGateway")
    
    # Constructs a dict of the graph. With as key the id - and the Name and Type as the values
    def __make_dict(self):
        self.dict_made = True
        self.__find_activities()
        self.__find_sub_processes()
        self.__find_choice_gateways()
        self.__find_parallel_gateways()         # Called after __find_choice_gateway since it overwrites attribute
        self.__find_message_activities()

        for activity in self.activities:
            self.activity_dict[activity.attrib['Id']] = {"Name": activity.attrib['Name'], "Type": activity.attrib["Type"],
                                                         "message_received_id": None, "message_received_name": None}

    # Constructs a graph of the relations between activities (id to (id, name)s)
    def __make_graph(self):
        self.graph_made = True
        self.__find_connections()

        for edge in self.connections:
            if "Name" in edge.attrib:
                self.connection_graph[edge.attrib['From']].append((edge.attrib['To'], edge.attrib["Name"]))
            else:
                self.connection_graph[edge.attrib['From']].append((edge.attrib['To'], ""))
        # Dict with {'From-id' : ['To-id', 'To2-id', ...]}

    # Returns a graph of the relations between id's (activities/gates)
    # Key: Id of a Node
    # Value: List of nodes that the key-node is connected to
    def get_graph(self):
        self.__make_graph()
        return self.connection_graph

    # Returns returns a hashmap with the ids as key and activities as values
    # Key: Id of a node
    # Value: Attributes of the Node
    def get_dict(self):
        if not self.dict_made:
            self.__make_dict()
        return self.activity_dict

    # Returns list of messages
    def get_messages(self):
        if not self.dict_made:  # Need the dictionary to be made, because it changes the "message_received" keys
            self.__make_dict()
        self.__find_messages()
        return self.messages

    # Return the id of the startNode
    def get_start_pair(self):
        start_tuple = self.__find_start_tuple()
        return start_tuple

    def get_subprocess(self):
        if not self.dict_made:
            self.__make_dict()
        return self.sub_processes

    # Return a list of the other startNodes and their Triggers
    def get_start_nodes(self):
        self.__find_all_start()
        return self.start

    # Returns the name of the diagram
    def get_name(self):
        return self.name
