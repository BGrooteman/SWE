'use strict';

const electron = require('electron');
const ChildProcess = require('child_process');
const portfinder = require('portfinder');
const app = electron.app;  // Module to control application life.
const BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform != 'darwin') {
    app.quit();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
	var py;
	portfinder.basePort = 5000
	portfinder.getPort(function (err, port) {
		console.log(port)
		py = ChildProcess.spawn('python3', ['guigenerator.py','flask:debug','flask:electron','flaskport:'+port])
		
		var mainAddr = 'http://localhost:'+port+'/';

		// Create the browser window.
		mainWindow = new BrowserWindow({width: 800, height: 600});
		
		// and load the index.html of the app.
		mainWindow.loadURL(mainAddr);

		// Open the DevTools.
		mainWindow.webContents.openDevTools();

		
		// Emitted when the window is closed.
		mainWindow.on('closed', function() {
			// Dereference the window object, usually you would store windows
			// in an array if your app supports multi windows, this is the time
			// when you should delete the corresponding element.
			mainWindow = null;
			process.stdout.write("hello: ");
			py.kill();
		});
	  });
});

