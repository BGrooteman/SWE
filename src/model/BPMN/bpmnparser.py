"""
Created on 18 March. 2016

@author: Team GuiGenerator

The parser will parse all the files separately and collect their information.
This had to be done since there are subprocesses that contain links to other diagrams,
therefore parsing every diagram separately would result in a lot of redirecting afterwards.
by first parsing every diagram seperately we can then establish the approriate links before
returning it to the translator.

Input:
    path: The path to the temp-folder
    nr_of_files: An integer with the number of files in the model
Output: 
get_diagram_info, which is a hash with these keys:
    Per diagram:
    "Graph": 		Directed Graph of the model in one diagram
    "Dict":			Dictionary with all the info of every Activity
    "Start_id":		Id of real StartNode
    "Start_nodes":	List of start nodes
    For all diagrams:
    "Sub_links":	List of the subprocess links
    "Messages": 	List of messages
"""
import logging
from collections import defaultdict
import model.BPMN.diagramparser as DP


class BpmnParser:
    def __init__(self, extractor):
        self.logger = logging.getLogger(__name__)

        # Store the number of diagrams that are going to be parsed.
        self.number_of_diagrams = extractor.get_nr_subfiles()
        
        self.list_nr_start = []              # List with the key: nr_of_file and value: id of StartNode
        self.list_start_nodes = []           # List of list of start nodes for every diagram
        self.list_of_graphs = []             # List of Graph with key: idOfStartNode and value: Graph
        self.list_of_dicts = []              # List of Dicts with key: nr_of_file and value: dict of that file
        self.subprocess_startNode_hash = {}  # Hash key: idSubprocess and value: idStartNode
        self.names = []                      # List of name for every diagram
        self.messages = {}                   # List of list of messages per diagram
        self.end_list = []                   # List of endNodes per diagram

        pool_startnode_hash = {}
        subprocess_hash = defaultdict(list)

        self._extract_all_diagrams(extractor, pool_startnode_hash, subprocess_hash)
        self.__match_hashes(subprocess_hash, pool_startnode_hash)

    # Loop over all the diagrams and use the DiagramParser to extract the information
    def _extract_all_diagrams(self, extractor, pool_startnode_hash, subprocess_hash):
        for diagram_path in extractor.get_paths():
            parser = DP.DiagramParser(diagram_path)
            pair = parser.get_start_pair() # Gets pair with (startNodeId, poolId)
            pool_startnode_hash[pair[1]] = pair[0]
            start_node = pair[0]
            self.list_nr_start.append(start_node)
            self.list_start_nodes.append(parser.get_start_nodes())
            self.list_of_dicts.append(parser.get_dict())
            self.list_of_graphs.append(parser.get_graph())
            self.names.append(parser.get_name())
            self.__store_messages(parser.get_messages())
            list_pair = parser.get_subprocess() # Gets list of pairs with (activityId, poolId)
            for item in list_pair:
                subprocess_hash[item[1]].append(item[0])

    # Matches two hashes with similar keys and links values.
    def __match_hashes(self, sub_process, pool_startnode):
        for key in pool_startnode:
            if key in sub_process:
                for reference in sub_process[key]:
                    self.subprocess_startNode_hash[reference] = pool_startnode[key]

    # Stores the messages in the returned list, in the self.messages. Duplicates can never occur so safely use .update
    def __store_messages(self, hash_of_messages):
        self.messages.update(hash_of_messages)

    # Returns Graph of nr_of_file
    def get_graph(self, index):
        return self.list_of_graphs[index]

    # Returns real start-element of nr_of_file
    def get_start(self, index):
        return self.list_nr_start[index]

    # Returns start-element of nr_of_file
    def get_start_nodes(self, index):
        return self.list_start_nodes[index]

    # Returns Dict of nr_file
    def get_dict(self, index):
        return self.list_of_dicts[index]

    # Returns a hash of the messages
    # Key: SourceId of Message
    # Value: {"Name": ..., "Target": ...}
    def get_messages(self):
        return self.messages

    # Returns hash of subprocess to startnode
    # Key: Id of subprocess
    # Value: Id of StartNode
    # So when a subprocess is reached, can find easily the start of graph by looking into the hash_of_graphs
    def get_sublink(self):
        return self.subprocess_startNode_hash

    # Returns list of names. Index is nr of the file (not in order)
    def get_name(self, index):
        return self.names[index]

    # Returns list of list of Id-end-nodes. Index is nr of the file (not in order)
    def get_end_list(self):
        return self.end_list

    # Returns a hash that contains all the information specific to one diagram
    def get_diagram_info(self, index):
        hash_info = {"Graph": self.get_graph(index),
                     "Dict": self.get_dict(index),
                     "Start_id": self.get_start(index),
                     "Start_nodes": self.get_start_nodes(index),
                     "Name": self.get_name(index),
                     "Sub_links": self.get_sublink(),
                     "Messages": self.get_messages(),
                     }
        return hash_info


