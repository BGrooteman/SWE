'''
Created on 14 Apr 2016

@author: Guigenerator
'''
import os
import werkzeug
import logging
import settings
import controller.inputHandler as inputhandler

logger = logging.getLogger(__name__)
logger_config_path = os.path.join(settings.PATH_CONFIG, 'logging.ini')
logger_path = settings.PATH_LOG

class FlaskController():
    
    def __init__(self,app):
        self.app = app

    #Saves bpm file to the server.
    def _save_bpm(self,bpm_file):
        file_name = werkzeug.utils.secure_filename(bpm_file.filename)
        bpm_file.save(os.path.join(self.app.config['UPLOAD_FOLDER'],file_name))
        return file_name
        
    #Creates the input and output folders if they don't exit.
    def _create_folders(self):
        input_file_path = self.app.config['UPLOAD_FOLDER']
        output_file_path = self.app.config['OUTPUT_FOLDER']
        os.makedirs(input_file_path, exist_ok=True)
        os.makedirs(output_file_path, exist_ok=True)
    
#Calls the input handler
def _handle_bpm_input(input_file_path,output_file_path,queue):
    #Get output name without path and filetype
    log_name = os.path.splitext(os.path.basename(output_file_path))[0]
    new_logger_path = os.path.join(logger_path,'guigenerator_'+log_name+'.log')
    logging.config.fileConfig(logger_config_path, disable_existing_loggers=False, defaults={'logfilename': new_logger_path})
    process_logger = logging.getLogger(__name__)
        
    inputhandler.handle_input(input_file_path, output_file_path)
    queue.put('Done')
    process_logger.debug('Finished handling input.')